#Author : Indrajith K L
#Licence : NONE - If you do modify credit me......

use_bpm 110


#bassloop1
live_loop :mainBass do
  with_fx :compressor do
    use_synth :chipbass
    sample :Bass_Loop_105bpm_F33, rate:-1
    sleep sample_duration(:Bass_Loop_105bpm_F33)
  end
end

#chords1
in_thread do
  loop do
    with_fx :reverb, room:1 do
      use_synth :chiplead
      sync :mainBass
      sample :Synth_Chords_Loop_105bpm_F33, rate:-1, pitch:5
      sleep sample_duration(:Synth_Chords_Loop_105bpm_F33)
    end
  end
end

#drum1
cutoffs = [30,50,70,80,90,100,120,0];
in_thread do
  loop do
    sync :mainBass
    with_fx :bitcrusher do
      t = cutoffs.rotate[0]
      puts t
      use_synth :chipbass
      sample :c_Drum_Loop_105bpm33, cutoff: 50, attack:5
      sleep sample_duration(:c_Drum_Loop_105bpm33)
    end
  end
end
